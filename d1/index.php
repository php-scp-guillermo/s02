<?php
require_once './code.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S2: Repetition Control Structure and Array Manipulation</title>
</head>

<body>
    <h1>Repetition Control Structures</h1>
    <h2>While-Loop</h2>
    <p><?php whileLoop(5); ?></p>

    <h2>Do While-Loop</h2>
    <p><?php doWhileLoop(10); ?></p>

    <h2>For-Loop</h2>
    <p><?php forLoop(10); ?></p>

    <h2>Continue and Break</h2>
    <p><?php modifiedForLoop(20); ?></p>

    <h1>Array Manipulation</h1>

    <h2>Types of Arrays:</h2>

    <h3>Simple Array</h3>
    <ul>
        <?php foreach ($grades as $grade) : ?>
            <li><?= $grade; ?>
            <?php endforeach ?>
    </ul>

    <h3>Associative Array</h3>
    <ul>
        <?php foreach ($gradePeriod as $key => $value) : ?>
            <li>value in <?= "$key is $grade"; ?>
            <?php endforeach ?>
    </ul>

    <h3>Multi-Dimensional Array</h3>
    <ul>
        <?php foreach ($hereos as $team)
            foreach ($team as $hero) : ?>
            <li><?= $hero; ?>
            <?php endforeach ?>
    </ul>

    <h3>Multi-Dimensional Associative Array </h3>
    <ul>
        <?php foreach ($powers as $key => $values)
            foreach ($values as $value) : ?>
            <li><?= "$key: $value"; ?>
            <?php endforeach ?>
    </ul>

    <h1>Array Functions</h1>
    <h3>Add to Array</h3>
    <?php array_push($computerBrands, 'Apple'); ?>
    <pre>
        <?php print_r($computerBrands) ?>
    </pre>

    <?php array_unshift($computerBrands, 'Apple'); ?>
    <pre>
        <?php print_r($computerBrands) ?>
    </pre>

    <h3>Remove from Array</h3>
    <?php array_pop($computerBrands); ?>
    <pre>
        <?php print_r($computerBrands) ?>
    </pre>

    <?php array_shift($computerBrands); ?>
    <pre>
        <?php print_r($computerBrands) ?>
    </pre>
    <h3>Array length</h3>
    <p><?= count($computerBrands) ?></p>
    <h3>In Array</h3>
    <p><?= searchBrand('acer', $computerBrands); ?></p>
</body>

</html>
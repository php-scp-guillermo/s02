<?php
// [SECTION] Repetition Control Structures 

//Repetition control structure or loops to execute code multiple times

//while Loop
function whileLoop(int $count): void
{

    while ($count !== 0) {
        echo $count . "<br />";
        $count--;
    }
}

//Do -while Loop
function doWhileLoop(int $count): void
{
    do {
        echo $count . "<br />";
        $count--;
    } while ($count > 0);
}

//For-Loop
function forLoop(int $count): void
{
    for ($i = 0; $i < $count; $i++) {
        echo $i . "<br />";
    }
}

//Continue and Break
//continue skip the current loop
//break stop the loop

function modifiedForLoop(int $count): void
{
    for ($i = 0; $i < $count; $i++) {
        if ($i % 2 === 0) continue;
        echo $i . "<br />";
        if ($i > 10) break;
    }
}


//[SECTION] Array Manipulation 
$studentNumber = array('1923', '1924', '1925', '1926');

$grades = [98.5, 94.3, 89.2, 90.1];

//Associative Arrays
$gradePeriod = [
    'firstGrading' => 98.5,
    'secondGrading' => 94.2,
    'thirdGrading' => 89.2,
    'fourthGrading' => 90.1
];

//Multi-dimensional Arrays
$hereos = [
    ['Iron Man', 'Thor', 'Hulk'],
    ['Wolverine', 'Cyclopes', 'Jean Grey'],
    ['Batman', 'Superman', 'Wonder Woman']
];

//Multi-dimensional Associative Array
$powers = [
    'regular' => ['Repulsor Blast', 'Rocket Punch'],
    'signature' => ['Unibeam']
];


//Array  Functions
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Searching arrays
function searchBrand(string $brand, array $brandArr): string
{
    if (in_array($brand, $brandArr)) {
        return "$brand is in the array!";
    } else {
        return "$brand is not in the Array";
    }
}

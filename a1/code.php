<?php
function printDivisbleByFive(int $num): void
{
    for ($i = 1; $i <= $num; $i++) {
        if ($i % 5 === 0) {
            echo $i . ",";
        }
    }
}

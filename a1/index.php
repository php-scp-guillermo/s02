<?php
require_once './code.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        div {
            border: 2px solid black;
            width: 500px;
            margin: 0 auto;
            padding: 0 0 1rem 0;
        }
    </style>
</head>

<body>
    <div>
        <h1>Divisble Of Five </h1>
        <?= printDivisbleByFive(100); ?>
    </div>
    <br>
    <div>
        <h1>Array Manipulation </h1>
        <?php
        $students = array();
        array_push($students, 'John Smith');
        var_dump($students);
        echo "<br/>";
        echo count($students);
        echo "<br/>";
        array_push($students, 'Jane Smith');
        var_dump($students);
        echo "<br/>";
        echo count($students);
        array_shift($students);
        echo "<br/>";
        var_dump($students);
        echo "<br/>";
        echo count($students);
        ?>
    </div>
</body>

</html>